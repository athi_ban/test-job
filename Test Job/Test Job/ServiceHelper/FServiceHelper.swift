//
//  FServiceHelper.swift
//  Test Job
//
//  Created by Athiban Ragunathan on 03/04/21.
//

import Foundation
import UIKit

enum CustomError : Error {
    case message(String)
    case inValidResponseFormat
    case offline(String)
}

class FServiceHelper {
    
    class func request<T: Codable>(router: FServiceManager, completion: @escaping (Result<T?, CustomError>) -> ()) {
        
        var components = URLComponents()
        components.scheme = router.scheme
        components.host = router.host
        components.path = router.path
    
        components.queryItems = router.parameters
        
        guard let url = components.url else { return }
                
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = router.method
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        ServiceHelper.instance.request(forUrlRequest: urlRequest, completion: { (result : Result<(Data, StatusCode), CustomError>) in
            
            DispatchQueue.main.async {
                
                switch result {
                case .success(let obj):
                    
                    switch obj.1 {
                
                    case .success:
                                      
                        let responseObject = try? JSONDecoder().decode(T.self, from: obj.0)
                        completion(.success(responseObject))
                        
                    case .notFound,.created,.unAuthorized,.notAcceptable,.unProcessableEntry,.badRequest,.internalServerError:
                        completion(.failure(.message("Server error. Please check the API.")))
                    }
                
                case .failure(let message): completion(.failure(message))
                }
            }
        })
    }
}
