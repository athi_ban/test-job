//
//  ServiceHelper.swift
//  Test Job
//
//  Created by Athiban Ragunathan on 03/04/21.
//

import Foundation

enum StatusCode : Int {
    case success = 200
    case created = 201
    case unAuthorized = 401
    case notFound = 404
    case notAcceptable = 406
    case unProcessableEntry = 422
    case badRequest = 400
    case internalServerError = 500
}

class ServiceHelper : NSObject {
    
    static let instance = ServiceHelper()
    
    func requestFormData(withData data : Data, forRequest request : URLRequest, completion: @escaping (Result<(Data, StatusCode), CustomError>) -> ()) {
        loadDataSession(forData: data, withURLRequest: request, completionHandler: {
            data, response, error in
            self.processResponse(urlRequest: request, data: data, response: response, error: error, completion: completion)
        })
    }
    
    func request(forUrlRequest urlRequest : URLRequest, completion: @escaping (Result<(Data, StatusCode), CustomError>) -> ()) {
        
        loadURLSession(withURLRequest: urlRequest, completionHandler: {
            data, response, error in
            self.processResponse(urlRequest: urlRequest, data: data, response: response, error: error, completion: completion)
        })
    }
    
    private func loadURLSession(withURLRequest request : URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let session = URLSession(configuration: .default)
        let dataTask = session.dataTask(with: request) { data, response, error in
            completionHandler(data,response,error)
        }
        
        dataTask.resume()
    }
    
    private func loadDataSession(forData data : Data, withURLRequest request : URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        session.uploadTask(with: request, from: data, completionHandler: { data, response, error in
            completionHandler(data,response,error)
        }).resume()
    }
    
    private func processResponse(urlRequest : URLRequest, data : Data?, response : URLResponse?, error : Error?, completion : @escaping (Result<(Data, StatusCode), CustomError>) -> ()) {
        
        if let httpResponse = response as? HTTPURLResponse, let statusCode = StatusCode(rawValue: httpResponse.statusCode), statusCode == .unAuthorized {
            return
        }
                
        guard error == nil else {
            completion(.failure(.message(error!.localizedDescription)))
            if let errorDesc = error?.localizedDescription {
                print(errorDesc)
            }
            return
        }
        
        guard let _data = data, let httpResponse = response as? HTTPURLResponse, let statusCode = StatusCode(rawValue: httpResponse.statusCode) else {
            completion(.failure(.message("Invalid/No response. Please check the API.")))
            return
        }
        
        completion(.success((_data, statusCode)))
        
    }
}
