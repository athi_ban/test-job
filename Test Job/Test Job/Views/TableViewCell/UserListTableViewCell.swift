//
//  UserListTableViewCell.swift
//  Test Job
//
//  Created by Athiban Ragunathan on 03/04/21.
//

import UIKit

class UserListTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var addresslabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var companylabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(user : UserModel) {
        
        nameLabel.text = user.name
        emailLabel.text = user.email
        addresslabel.text = "\(user.address.suite), \(user.address.street), \(user.address.city), \(user.address.zipcode)"
        phoneLabel.text = user.phone
        websiteLabel.text = user.website
        companylabel.text = "\(user.company.name), \(user.company.catchPhrase), \(user.company.bs)"
    }
}
