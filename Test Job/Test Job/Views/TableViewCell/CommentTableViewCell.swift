//
//  CommentTableViewCell.swift
//  Test Job
//
//  Created by Athiban Ragunathan on 03/04/21.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var comment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func configureCell(comment : CommentModel) {
        nameLabel.text = comment.name
        emailLabel.text = comment.email
        self.comment.text = comment.body
    }
}
