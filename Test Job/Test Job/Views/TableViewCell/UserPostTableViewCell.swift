//
//  UserPostTableViewCell.swift
//  Test Job
//
//  Created by Athiban Ragunathan on 03/04/21.
//

import UIKit
import Kingfisher

class UserPostTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var commentsButton: UIButton!
    
    var post : PostModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func configureCell(post : PostModel) {
        titleLabel.text = post.title
        descriptionLabel.text = post.body
        if let url = post.image {
            postImageView.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "placeholder"))
        }
        else {
            getPostPhotos(id: post.id)
        }
    }
    
    func getPostPhotos(id : Int) {
        
        FServiceHelper.request(router: .getPostPhotos(id: id), completion: { (result : Result<[PhotoModel]?, CustomError>) in
            
            DispatchQueue.main.async {
                                
                switch result {
                case .success(let response):
                    if self.post.id == id, let urlString = response?.first?.thumbnailURL {
                        self.post.image = urlString
                        self.postImageView.kf.setImage(with: URL(string: urlString), placeholder: UIImage(named: "placeholder"))
                    }
                case .failure(let message): print("\(message)")
                }
            }
        })
    }
}
