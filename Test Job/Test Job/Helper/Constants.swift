//
//  Constants.swift
//  Test Job
//
//  Created by Athiban Ragunathan on 03/04/21.
//

import Foundation

struct API {
    static var baseURL = "jsonplaceholder.typicode.com"
    static var scheme = "https"
}

extension API {
    
    static var fullBaseUrl : String {
        get {
            return API.scheme + "://" + API.baseURL
        }
    }
}
