//
//  CommentsViewModel.swift
//  Test Job
//
//  Created by Athiban Ragunathan on 03/04/21.
//

import Foundation

protocol CommentsDelegate {
    func success()
}

class CommentsViewModel {
    
    var delegate : CommentsDelegate?
    var commentsArray : [CommentModel] {
        get {
            guard let commentsArrayObj = _commentsArray else { return [] }
            return commentsArrayObj
        }
    }

    private var _commentsArray : [CommentModel]?
    
    func getComments(id : Int) {
        
        FServiceHelper.request(router: .getComments(id: id), completion: { (result : Result<[CommentModel]?, CustomError>) in
            
            DispatchQueue.main.async {
                                
                switch result {
                case .success(let response):
                    self._commentsArray = response
                    self.delegate?.success()
                case .failure(let message): print("\(message)")
                }
            }
        })
    }
}
