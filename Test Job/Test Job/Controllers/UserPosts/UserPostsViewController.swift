//
//  UserPostsViewController.swift
//  Test Job
//
//  Created by Athiban Ragunathan on 03/04/21.
//

import UIKit

class UserPostsViewController: UIViewController {

    @IBOutlet weak var postsTableView: UITableView!
    
    lazy var viewModel: UserPostsViewModel = {
        return UserPostsViewModel()
    }()
    
    var user : UserModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.delegate = self
        viewModel.getUserPosts(id: user.id)
        
        title = "Posts"

        postsTableView.register(UINib(nibName: "UserPostTableViewCell", bundle: nil), forCellReuseIdentifier: "UserPostTableViewCell")
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Action
    
    @objc func commentsAction(sender : UIButton) {
        performSegue(withIdentifier: "postToComments", sender: viewModel.userPostsArray[sender.tag])
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? CommentsViewController, let post = sender as? PostModel {
            vc.post = post
        }
    }
}

extension UserPostsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.userPostsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserPostTableViewCell") as! UserPostTableViewCell
        cell.commentsButton.tag = indexPath.row
        cell.post = viewModel.userPostsArray[indexPath.row]
        cell.configureCell(post: viewModel.userPostsArray[indexPath.row])
        
        cell.commentsButton.addTarget(self, action: #selector(commentsAction(sender:)), for: .touchUpInside)
        return cell
    }
}

extension UserPostsViewController : UserPostsDelegate {
    
    func success() {
        postsTableView.reloadData()
    }
}
