//
//  UserPostsViewModel.swift
//  Test Job
//
//  Created by Athiban Ragunathan on 03/04/21.
//

import Foundation

protocol UserPostsDelegate {
    func success()
}

class UserPostsViewModel {
    
    var delegate : UserPostsDelegate?
    var userPostsArray : [PostModel] {
        get {
            guard let userPostsArrayObj = _userPostsArray else { return [] }
            return userPostsArrayObj
        }
    }

    private var _userPostsArray : [PostModel]?

    func getUserPosts(id : Int) {
        
        FServiceHelper.request(router: .getUserPosts(id: id), completion: { (result : Result<[PostModel]?, CustomError>) in
            
            DispatchQueue.main.async {
                                
                switch result {
                case .success(let response):
                    self._userPostsArray = response
                    self.delegate?.success()
                case .failure(let message): print("\(message)")
                }
            }
        })
    }
}
