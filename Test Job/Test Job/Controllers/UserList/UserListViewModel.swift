//
//  UserListViewModel.swift
//  Test Job
//
//  Created by Athiban Ragunathan on 03/04/21.
//

import Foundation
import UIKit

protocol UserListDelegate {
    func success()
}

class UserListViewModel {
    
    var delegate : UserListDelegate?
    var userListArray : [UserModel] {
        get {
            guard let userListArrayObj = _userListArray else { return [] }
            return userListArrayObj
        }
    }
    
    private var _userListArray : [UserModel]?
    
    func retriveUserList() {
        
        FServiceHelper.request(router: .getUserList, completion: { (result : Result<[UserModel]?, CustomError>) in
            
            DispatchQueue.main.async {
                                
                switch result {
                case .success(let response):
                    self._userListArray = response
                    self.delegate?.success()
                case .failure(let message): print("\(message)")
                }
            }
        })
    }
}
