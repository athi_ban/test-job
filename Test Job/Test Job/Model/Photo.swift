//
//  Photo.swift
//  Test Job
//
//  Created by Athiban Ragunathan on 03/04/21.
//

import Foundation

// MARK: - UserElement
struct PhotoModel: Codable {
    let albumID, id: Int
    let title: String
    let url, thumbnailURL: String

    enum CodingKeys: String, CodingKey {
        case albumID = "albumId"
        case id, title, url
        case thumbnailURL = "thumbnailUrl"
    }
}
