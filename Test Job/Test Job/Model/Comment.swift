//
//  Comment.swift
//  Test Job
//
//  Created by Athiban Ragunathan on 03/04/21.
//

import Foundation

// MARK: - UserElement
struct CommentModel: Codable {
    let postID, id: Int
    let name, email, body: String

    enum CodingKeys: String, CodingKey {
        case postID = "postId"
        case id, name, email, body
    }
}
