//
//  Posts.swift
//  Test Job
//
//  Created by Athiban Ragunathan on 03/04/21.
//

import Foundation

// MARK: - UserElement
struct PostModel: Codable {
    let userID, id: Int
    let title, body: String
    var image: String?

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id, title, body
    }
}
