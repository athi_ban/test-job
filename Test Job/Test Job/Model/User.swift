//
//  User.swift
//  Test Job
//
//  Created by Athiban Ragunathan on 03/04/21.
//

import Foundation

// MARK: - UserElement
struct UserModel: Codable {
    let id: Int
    let name, username, email: String
    let address: AddressModel
    let phone, website: String
    let company: CompanyModel
}

// MARK: - Address
struct AddressModel: Codable {
    let street, suite, city, zipcode: String
    let geo: GeoModel
}

// MARK: - Geo
struct GeoModel: Codable {
    let lat, lng: String
}

// MARK: - Company
struct CompanyModel: Codable {
    let name, catchPhrase, bs: String
}
