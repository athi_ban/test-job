//
//  FServiceManager.swift
//  Test Job
//
//  Created by Athiban Ragunathan on 03/04/21.
//

import Foundation

enum ContentType : String {
    case formData = "multipart/form-data"
    case json = "application/json"
    case x_www_form_urlEncoded = "application/x-www-form-urlencoded"
}

enum FServiceManager {
    
    case getUserList
    case getUserPosts(id : Int)
    case getPostPhotos(id : Int)
    case getComments(id : Int)

    var scheme: String {
        switch self {
        case .getUserList,.getUserPosts,.getPostPhotos,.getComments: return API.scheme
        }
    }
    
    var host: String {
        switch self {
        case .getUserList,.getUserPosts,.getPostPhotos,.getComments: return API.baseURL
        }
    }
    
    var path: String {
        switch self {
        case .getUserList : return "/users"
        case .getUserPosts : return "/posts"
        case .getPostPhotos : return "/photos"
        case .getComments : return "/comments"
        }
    }
    
    var method: String {
        switch self {
        case .getUserList,.getUserPosts,.getPostPhotos,.getComments: return "GET"
        }
    }
    
    var parameters: [URLQueryItem]? {
        switch self {
        case .getUserList:
            return nil
        case .getUserPosts(let id):
            return [URLQueryItem(name: "userId", value: "\(id)")]
        case .getPostPhotos(let id):
            return [URLQueryItem(name: "id", value: "\(id)")]
        case .getComments(let id):
            return [URLQueryItem(name: "postId", value: "\(id)")]
        }
    }
}
